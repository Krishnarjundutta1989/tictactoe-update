//
//  ViewController.m
//  Test2TicTacToe
//
//  Created by Anil Kumar on 9/22/15.
//  Copyright (c) 2015 Anil Kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    
    NSMutableArray *arrayUserSelectedButtons;
    NSMutableArray *arrayAutoSelectedButtons;
    NSMutableArray *arrayAllSelectedButtons;
    
    NSMutableArray  *btnArray;
    UIImageView * defaultImageView;
    NSString *num;
    
    int j;
    BOOL isWinner;
    
    
    
    int w1 ;
    int w2 ;
    int w3 ;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    {
        
        btnArray = [NSMutableArray new];
        
        
        w1  =1;
        w2  =1;
        w3  =1;
        arrayUserSelectedButtons = [NSMutableArray new];
        
        UIImageView *imgTicTac = [UIImageView new];
        imgTicTac.frame = CGRectMake(20, 150, 350, 320);
        imgTicTac.image = [UIImage imageNamed:@"puzzle_game_best_tic_tac_toe_for_iphone_ipod_touch_and_ipad_1.png"];
        [self.view addSubview:imgTicTac];
        
        int xPos = 70;
        int yPos = 180;
        
        for (int i=1; i<=9; i++) {
            
            UIView *viewTicTac = [UIView new];
            viewTicTac.frame = CGRectMake(xPos, yPos, 67, 80);
            viewTicTac.backgroundColor = [UIColor redColor];
            
            UIButton *btnTicTac = [UIButton new];
            btnTicTac.frame = CGRectMake(0, 0, 67, 80);
            btnTicTac.backgroundColor = [UIColor blueColor];
            btnTicTac.tag = i;
            [btnTicTac addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            xPos = xPos + 83;
            
            if (i%3 == 0) {
                
                xPos = 70;
                yPos = yPos + 95;
            }
            [self.view addSubview:viewTicTac];
            [viewTicTac addSubview:btnTicTac];
            [btnArray addObject:btnTicTac];
        }
    }
}

- (BOOL)checkUserInputNumber:(NSString *)firstNumber secondNumber:(NSString *)secondNumber {
    
    // chk condition for 1
    if (([firstNumber integerValue] == 2 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 3 && [secondNumber integerValue] == 2) || ([firstNumber integerValue] == 4 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 4) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 5))
        
    {
        
        return TRUE;
    }
    // chk condition for 2
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 3 && [secondNumber integerValue] == 1) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 8) || ([firstNumber integerValue] == 8 && [secondNumber integerValue] == 5))
        
    {
        
        return TRUE;
    }
    // chk condition for 3
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 2) || ([firstNumber integerValue] == 2 && [secondNumber integerValue] == 1) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 6 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 5) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 6))
        
    {
        
        return TRUE;
    }
    // chk condition for 4
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 6) || ([firstNumber integerValue] == 6 && [secondNumber integerValue] == 5) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 1))
        
    {
        
        return TRUE;
    }
    // chk condition for 5
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 2 && [secondNumber integerValue] == 8) || ([firstNumber integerValue] == 3 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 4 && [secondNumber integerValue] == 6) ||([firstNumber integerValue] == 6 && [secondNumber integerValue] == 4) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 8 && [secondNumber integerValue] == 2) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 1))
        
    {
        
        return TRUE;
    }
    // chk condition for 6
    if (([firstNumber integerValue] == 3 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 4 && [secondNumber integerValue] == 5) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 4) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 3))
        
    {
        
        return TRUE;
    }
    // chk condition for 7
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 4) || ([firstNumber integerValue] == 3 && [secondNumber integerValue] == 5) || ([firstNumber integerValue] == 4 && [secondNumber integerValue] == 1) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 8 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 8))
        
    {
        
        return TRUE;
    }
    
    // chk condition for 8
    if (([firstNumber integerValue] == 2 && [secondNumber integerValue] == 5) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 2) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 7))
        
    {
        
        return TRUE;
    }
    // chk condition for 9
    if (([firstNumber integerValue] == 3 && [secondNumber integerValue] == 6) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 1) || ([firstNumber integerValue] == 6 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 8) || ([firstNumber integerValue] == 8 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 1 && [secondNumber integerValue] == 5))
        
    {
        
        return TRUE;
    }
    
    
    return NO;
}

- (NSInteger)getNextAutoGeneratedNumber:(NSString *)firstNumber secondNumber:(NSString *)secondNumber  {
    
    if (([firstNumber integerValue] == 2 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 3 && [secondNumber integerValue] == 2) || ([firstNumber integerValue] == 4 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 4) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 5))
    {
        return 1;
    }
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 3 && [secondNumber integerValue] == 1) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 8) || ([firstNumber integerValue] == 8 && [secondNumber integerValue] == 5))
    {
        return  2;
    }
    
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 2) || ([firstNumber integerValue] == 2 && [secondNumber integerValue] == 1) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 6 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 5) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 6))
    {
        return 3;
    }
    
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 6) || ([firstNumber integerValue] == 6 && [secondNumber integerValue] == 5) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 1))
    {
        return  4;
    }
    
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 2 && [secondNumber integerValue] == 8) || ([firstNumber integerValue] == 3 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 4 && [secondNumber integerValue] == 6) ||([firstNumber integerValue] == 6 && [secondNumber integerValue] == 4) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 8 && [secondNumber integerValue] == 2) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 1))
    {
        return  5;
    }
    
    if (([firstNumber integerValue] == 3 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 4 && [secondNumber integerValue] == 5) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 4) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 3))
    {
        return  6;
    }
    
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 4) || ([firstNumber integerValue] == 3 && [secondNumber integerValue] == 5) || ([firstNumber integerValue] == 4 && [secondNumber integerValue] == 1) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 8 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 8))
    {
        
        return 7;
    }
    if (([firstNumber integerValue] == 2 && [secondNumber integerValue] == 5) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 2) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 9 && [secondNumber integerValue] == 7))
    {
        return  8;
    }
    if (([firstNumber integerValue] == 3 && [secondNumber integerValue] == 6) || ([firstNumber integerValue] == 5 && [secondNumber integerValue] == 1) || ([firstNumber integerValue] == 6 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 8) || ([firstNumber integerValue] == 8 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 1 && [secondNumber integerValue] == 5))
    {
        
        return 9;
    }
    
    
    
    
    return -1;
}

- (BOOL)checkUserThirdInputNumber:(NSString *)firstNumber secondNumber:(NSString *)secondNumber thirdNumber: (NSString *)selected3rdValue {
    
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 2 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 4 && [secondNumber integerValue] == 5 && [secondNumber integerValue] == 6) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 8 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 1 && [secondNumber integerValue] == 4 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 2 && [secondNumber integerValue] == 5 && [secondNumber integerValue] == 8) || ([firstNumber integerValue] == 3 && [secondNumber integerValue] == 6 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 1 && [secondNumber integerValue] == 5 && [secondNumber integerValue] == 9) ||([firstNumber integerValue] == 3 && [secondNumber integerValue] == 5 && [secondNumber integerValue] == 7))
        
    {
        
        return TRUE;
    }
    return FALSE;
}
- (void)getNextAutoGeneratedNumberforthird:(NSString *)firstNumber secondNumber:(NSString *)secondNumber thirdNumber: (NSString *)selected3rdValue  {
   
    if (([firstNumber integerValue] == 1 && [secondNumber integerValue] == 2 && [secondNumber integerValue] == 3) || ([firstNumber integerValue] == 4 && [secondNumber integerValue] == 5 && [secondNumber integerValue] == 6) || ([firstNumber integerValue] == 7 && [secondNumber integerValue] == 8 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 1 && [secondNumber integerValue] == 4 && [secondNumber integerValue] == 7) || ([firstNumber integerValue] == 2 && [secondNumber integerValue] == 5 && [secondNumber integerValue] == 8) || ([firstNumber integerValue] == 3 && [secondNumber integerValue] == 6 && [secondNumber integerValue] == 9) || ([firstNumber integerValue] == 1 && [secondNumber integerValue] == 5 && [secondNumber integerValue] == 9) ||([firstNumber integerValue] == 3 && [secondNumber integerValue] == 5 && [secondNumber integerValue] == 7))
    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"WON" message:@"You WON" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    }
}

- (void)btnClicked:(UIButton *)sender {
    
    [arrayUserSelectedButtons addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
    
    if ([arrayUserSelectedButtons count] >= 2 ) {
        
        for (int i= 0; i< [arrayUserSelectedButtons count] ; i++) {
            
            NSString *selected1stValue = [arrayUserSelectedButtons objectAtIndex:i];
            NSString *selected2ndValue = [arrayUserSelectedButtons objectAtIndex:++i];
            
            if ([self checkUserInputNumber:selected1stValue secondNumber:selected2ndValue
                 ] == TRUE) {
                
                NSInteger newAutoButtonTag = [self getNextAutoGeneratedNumber:selected1stValue secondNumber:selected2ndValue];
                --newAutoButtonTag;
                
                UIButton *btn = (UIButton *)[btnArray objectAtIndex:newAutoButtonTag];
                [btn setImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
            }
        }
    }
    if ([arrayUserSelectedButtons count] >= 3 ) {
        
        for (int i= 0; i< [arrayUserSelectedButtons count] ; i++) {
            
            NSString *selected1stValue = [arrayUserSelectedButtons objectAtIndex:i];
            NSString *selected2ndValue = [arrayUserSelectedButtons objectAtIndex:++i];
            NSString *selected3rdValue = [arrayUserSelectedButtons objectAtIndex:++i];
            
            if ([self checkUserThirdInputNumber:selected1stValue secondNumber:selected2ndValue thirdNumber : selected3rdValue
                 ] == TRUE) {
                
//                NSInteger newAutoButtonTag = [self getNextAutoGeneratedNumberforthird:selected1stValue secondNumber:selected2ndValue thirdNumber : selected3rdValue];
//                --newAutoButtonTag;
//                
//                UIButton *btn = (UIButton *)[btnArray objectAtIndex:newAutoButtonTag];
//                [btn setImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
            }
        }
    }

    
    if ([arrayUserSelectedButtons count] == 1) {
        
        UIButton *btn = (UIButton *) sender;
        [btn setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
        
        [self randamNumber:btn];
    }
    else {
        
        UIButton *btn = (UIButton *) sender;
        [btn setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    }
}


- (NSInteger)randomValueBetween:(NSInteger)min and:(NSInteger)max {
    return (NSInteger)(1 + arc4random_uniform(9 - 1 + 1));
}

-(void)randamNumber: (id) sender
{
    
    NSInteger randNum = [self randomValueBetween:1 and:9];
    
    num = [NSString stringWithFormat:@"%ld", randNum]; //Make the number into a string.
    if ([sender tag]!=randNum) {
        [arrayAutoSelectedButtons addObject:[NSString stringWithFormat:@"%ld",randNum]];
        NSLog(@"%lu", (unsigned long)arrayUserSelectedButtons.count);
        UIButton *btn = (UIButton *) [btnArray objectAtIndex:randNum-1];
        [btn setImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
        [arrayAutoSelectedButtons addObject:[NSString stringWithFormat:@"%ld",randNum]];
        NSLog(@"%lu", (unsigned long)arrayUserSelectedButtons.count);
    }
    else{
        [arrayAutoSelectedButtons addObject:[NSString stringWithFormat:@"%ld",(long)randNum]];
        UIButton *btn = (UIButton *) [btnArray objectAtIndex:randNum+1];
        [btn setImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
        [arrayAutoSelectedButtons addObject:[NSString stringWithFormat:@"%ld",randNum]];
        NSLog(@"%lu", (unsigned long)arrayUserSelectedButtons.count);
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
